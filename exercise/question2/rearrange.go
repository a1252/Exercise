package question2

type ByteFreq struct {
	char byte
	freq uint
}

/*
@param: a map of each byte in the string with their frequency
@returns: a struct consisting of the byte with the highest frequency
*/
func FindBigValue(freq map[byte]uint) ByteFreq {
	var frequent ByteFreq
	for k, v := range freq {
		if v > frequent.freq {
			frequent.char = k
			frequent.freq = v
		}
	}
	return frequent
}

/*
@returns: a slice containing a struct with each value and their frequency in sorted order and
a boolean showing if it is possible to arrange the string or not
*/
func checkPossibility(str string) ([]ByteFreq, bool) {
	freq := make(map[byte]uint)

	for i := 0; i < len(str); i++ {
		freq[str[i]]++
	}

	for _, v := range freq {
		half := len(str) / 2
		if len(str)%2 == 1 {
			half++
		}
		if v > uint(half) {
			return nil, false
		}
	}

	var result []ByteFreq
	for i := 0; i < len(freq); i++ {
		frequent := FindBigValue(freq)
		result = append(result, frequent)
		freq[frequent.char] = 0
	}

	return result, true
}

func RearrangeStr(str string) string {
	// check if it possible to rearrange the string
	result, ok := checkPossibility(str)
	if !ok {
		return ""
	}

	resultStr := make([]byte, len(str))
	idx := 0

	for _, frequency := range result {
		for i := 0; i < int(frequency.freq); i++ {
			resultStr[idx] = frequency.char
			idx += 2
			if idx >= len(resultStr) {
				idx = 1
			}
		}

	}
	return string(resultStr)
}
