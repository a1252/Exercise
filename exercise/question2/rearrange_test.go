package question2

import "testing"

func IsArranged(str string) bool {
	for i := 0; i < len(str)-1; i++ {
		if str[i] == str[i+1] {
			return false
		}
	}
	return true
}

func TestRearrangeStr(t *testing.T) {
	tests := []string{
		"aab", "", "aaabacbcbbb", "aaab", "aaaabbcc", "aaa", "aaabbbccde",
	}

	for _, test := range tests {
		result := RearrangeStr(test)
		if !IsArranged(result) {
			t.Errorf("Wrong arrangement")
		}
	}
}
