package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"question1/test"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/joho/godotenv"
)

var (
	Ctx     context.Context
	Queries *test.Queries
	Db      *pgxpool.Pool
)

func createUser(c *gin.Context) {
	tx, err := Db.Begin(Ctx)

	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback(Ctx)
	qtx := Queries.WithTx(tx)

	user := struct {
		Name         string `json:"name"`
		Phone_Number string `json:"phone_number"`
	}{}

	if err := c.BindJSON(&user); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "Malformed payload"})
		return
	}

	if len(user.Name) == 0 {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "Name shouldn't be empty"})
		return
	}

	_, err = qtx.GetUsersByPhoneNumber(Ctx, user.Phone_Number)
	// check if user already exists
	if err == nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "User exists"})
		return
	}

	inserted, err := qtx.CreateUser(Ctx, test.CreateUserParams{
		Name:        user.Name,
		PhoneNumber: user.Phone_Number,
	})

	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "Query failed"})
		return
	}

	_ = inserted
	c.IndentedJSON(http.StatusOK, gin.H{"success": "user created"})
	tx.Commit(Ctx)
}

func generateRandomString() string {
	var res string

	for i := 0; i < 4; i++ {
		res += strconv.Itoa(rand.Intn(9))
	}
	return res
}

func generateOtp(c *gin.Context) {
	tx, err := Db.Begin(Ctx)

	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback(Ctx)
	qtx := Queries.WithTx(tx)

	otp := struct {
		Phone_number string `json:"phone_number"`
	}{}

	if err := c.BindJSON(&otp); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "Malformed payload"})
		return
	}

	_, err = qtx.GetUsersByPhoneNumber(Ctx, otp.Phone_number)
	if err != nil {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": "User doesn't exist"})
		return
	}

	id, err := Queries.SetOtpExpirationTime(Ctx, test.SetOtpExpirationTimeParams{
		Otp:               pgtype.Text{String: generateRandomString(), Valid: true},
		OtpExpirationTime: pgtype.Timestamp{Time: time.Now().Add(time.Minute), Valid: true},
		PhoneNumber:       otp.Phone_number})

	if err != nil {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": "Error occured while querying"})
		return
	}
	_ = id
	tx.Commit(Ctx)
}

func verifyOtp(c *gin.Context) {
	tx, err := Db.Begin(Ctx)

	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback(Ctx)
	qtx := Queries.WithTx(tx)

	payload := struct {
		Phone_number string `json:"phone_number"`
		Otp          string `json:"otp"`
	}{}

	if err := c.BindJSON(&payload); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "Malformed payload"})
		return
	}

	user, err := qtx.GetUsersByPhoneNumber(Ctx, payload.Phone_number)
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "User doesn't exist"})
		return
	}

	if time.Since(user.OtpExpirationTime.Time) > 0 {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "OTP has expired"})
		return
	}

	if user.Otp.String != payload.Otp {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"error": "Incorrect OTP"})
		return
	}
	c.IndentedJSON(http.StatusOK, gin.H{"success": "Valid OTP"})
	tx.Commit(Ctx)
}

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	Ctx = context.Background()
	// fmt.Sprintf("postgres://%v:%v@%v:5432/%v", os.Getenv("POSTGRES_USER"),
	// os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_DB"))
	conn, err := pgxpool.New(Ctx, fmt.Sprintf("postgres://%v:%v@%v:5432/%v", os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_DB")))
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	Db = conn

	Queries = test.New(conn)

	route := gin.Default()

	route.POST("/api/users", createUser)
	route.POST("/api/users/generateotp", generateOtp)
	route.POST("/api/users/verifyotp", verifyOtp)

	route.Run(":80")
}
