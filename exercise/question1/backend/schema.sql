CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	phone_number VARCHAR(255) UNIQUE NOT NULL,
	otp VARCHAR(255),
	otp_expiration_time TIMESTAMP
);
