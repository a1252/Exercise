// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.25.0

package test

import (
	"github.com/jackc/pgx/v5/pgtype"
)

type User struct {
	ID                int32
	Name              string
	PhoneNumber       string
	Otp               pgtype.Text
	OtpExpirationTime pgtype.Timestamp
}
