-- name: CreateUser :one
INSERT INTO users (name, phone_number) VALUES ($1, $2) RETURNING id;

-- name: ListUsers :many
SELECT * FROM users;

-- name: GetUsersByPhoneNumber :one
SELECT * FROM users WHERE phone_number = $1;

-- name: SetOtpExpirationTime :one
UPDATE users SET otp = $1, otp_expiration_time = $2 WHERE phone_number = $3 RETURNING id;

-- name: GetOtpExpirationTime :one
SELECT otp_expiration_time FROM users where phone_number = $1;
