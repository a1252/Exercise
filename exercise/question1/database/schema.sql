\c postgres;

DROP DATABASE IF EXISTS test;

CREATE DATABASE test;

\c test;

CREATE TABLE IF NOT EXISTS users (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	phone_number VARCHAR(255) UNIQUE NOT NULL,
	otp VARCHAR(255),
	otp_expiration_time TIMESTAMP
);
