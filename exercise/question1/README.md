# Running

```bash
make build && make start
```

## testing an endpoint
```sh
curl -X POST -H "Content-Type: application/json" localhost/api/users -d '{"name": "Akeak", "phone_number": "123456"}'
```