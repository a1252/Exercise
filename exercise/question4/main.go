package main

import (
	"context"
	"fmt"
	"time"

	"math/rand"
	"sync"
)

var mu sync.Mutex

func generateRandPrintableByte() byte {
	var printableBytes []byte

	for i := 33; i < 126; i++ {
		printableBytes = append(printableBytes, byte(i))
	}

	return printableBytes[rand.Intn(len(printableBytes))]
}

func Runner(timeout time.Duration, consumers int, producers int) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	buffer := make(chan []byte, 1)
	var byteSlice []byte

	// writer
	for i := 0; i < producers; i++ {
		go func(buf chan<- []byte, shared *[]byte) {
			for {
				val := generateRandPrintableByte()
				mu.Lock()
				buffer <- *shared
				*shared = append(*shared, val)
				mu.Unlock()
				fmt.Println("Produced:", val)
			}
		}(buffer, &byteSlice)
	}

	// reader
	for i := 0; i < consumers; i++ {
		go func(buf <-chan []byte) {
			for {
				newBuf := <-buf
				fmt.Println("updated Buffer:", string(newBuf))
			}
		}(buffer)
	}

	select {
	case <-ctx.Done():
		return fmt.Errorf("timed out")
	}
}

func main() {
	timeout := time.Second * 2

	Runner(timeout, 8, 2)
	Runner(timeout, 8, 8)
	Runner(timeout, 8, 16)
	Runner(timeout, 2, 8)
}
