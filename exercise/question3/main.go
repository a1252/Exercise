package main

import "fmt"

func SwapSeats(users map[int]string) map[int]string {
	swaps := 0
	if len(users)%2 == 0 {
		swaps = len(users)
	} else {
		swaps = len(users) - 1
	}
	for i := 0; i < swaps; i += 2 {
		tmp := users[i+1]
		users[i+1] = users[i+2]
		users[i+2] = tmp
	}
	return users
}

func main() {
	users := make(map[int]string)

	users[1] = "Abbot"
	users[2] = "Doris"
	users[3] = "Emerson"
	users[4] = "Green"
	users[5] = "James"
	fmt.Println(SwapSeats(users))
}
